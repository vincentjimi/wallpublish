<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WallController;
use App\Http\Controllers\PublicationController;
use App\Http\Controllers\FollowController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\ChapterController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\PageController;
use App\Mail\NewUserWelcomeMail;
// use App\Http\Controllers\Auth\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/about', function(){
    return view('about');
});

Auth::routes();


Route::get('/contact', [ContactController::class, 'contact'])->name('contact');
Route::get('/send-mail', [ContactController::class, 'sendEmail'])->name('contact.send');

Route::get('/', [IndexController::class, 'index'] );


Route::post('follow/{user}', [FollowController::class, 'store']);
Route::get('/search', [PublicationController::class, 'search']);

Route::get('/home', [HomeController::class, 'index'])->name('home'); 



Route::post('/publication/{publication}/chapter/{chapter}/add-pages', [ChapterController::class, 'upload'])->name('pages.store');
// Route::get('/publication/{publication}/chapter/{chapter}/page/{page}/edit', [PageController::class, 'edit'])->name('page.edit');
Route::post('/publication/{publication}/chapter/{chapter}/page/{page}/update', [PageController::class, 'update'])->name('page.update');
Route::get('/publication/{publication}/chapter/{chapter}/page/{page}/destroy', [PageController::class, 'destroy'])->name('page.destroy');

Route::post('/publication/{publication}/chapter', [ChapterController::class, 'store']);
Route::get('/publication/{publication}/chapter/{chapter}', [ChapterController::class, 'show'])->name('chapter.show');
Route::get('/publication/{publication}/chapter/{chapter}/edit', [ChapterController::class, 'edit'])->name('chapter.edit');
Route::patch('/publication/{publication}/chapter/{chapter}', [ChapterController::class, 'update'])->name('chapter.update');
Route::get('/publication/{publication}/chapter/{chapter}/destroy', [ChapterController::class, 'destroy'])->name('chapter.destroy ');

Route::get('/publication/create', [PublicationController::class, 'create'])->name('publication.create');
Route::post('/publication', [PublicationController::class, 'store']);
Route::get('/publication/{publication}', [PublicationController::class, 'show'])->name('publication.show');
Route::get('/publication/{publication}/edit', [PublicationController::class, 'edit'])->name('publication.edit');
Route::patch('/publication/{publication}', [PublicationController::class, 'update'])->name('publication.update');
Route::get('/publication/{publication}/destroy', [PublicationController::class, 'destroy'])->name('publication.destroy');

Route::get('/wall/{user}', [WallController::class, 'index'])->name('wall.show');
Route::get('/wall/{user}/edit', [WallController::class, 'edit'])->name('wall.edit');
Route::patch('/wall/{user}', [WallController::class, 'update'])->name('wall.update');

