<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('walls', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('display_picture')->nullable();
            $table->string('wall_title')->nullable();
            $table->text('bio')->nullable();
            $table->boolean('manga')->default(0);
            $table->boolean('comics')->default(0);
            $table->boolean('belgian_comics')->default(0);
            $table->boolean('comedy')->default(0);
            $table->boolean('action')->default(0);
            $table->boolean('adventure')->default(0);
            $table->boolean('gore')->default(0);
            $table->boolean('fantasy')->default(0);
            $table->boolean('sci_fi')->default(0);
            $table->boolean('sport')->default(0);
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('walls');
    }
}
