@extends('layouts.app')

@section('content')
<div class="container" data-barba="container" data-barba-namespace="home">

<div class="row flex-md-reverse ">
    <div class="form-col col-lg-6 col-12 text-secondary pb-5 pb-lg-0">
        <h1 class="py-4">Register</h1>
        @guest
            @if (Route::has('login'))       
                <div class="col-8 offset-4 d-flex flex-row-reverse"><small>
                    <div class="d-flex align-items-center flex-row">
                        Already have an acount?</small> <strong><a class="text-right nav-link a pr-0" href="{{ route('login') }}">{{ __('Login') }}</a></strong>
                    </div>
                </div>
            @endif
            
        @endguest
        
        <form method="POST" action="{{ route('register') }}">
            @csrf
            
            <div class="form-group row">

                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                <div class="col-md-8">
                    <input id="name" type="text" class="rounded-pill form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}"  autocomplete="name" autofocus>

                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            
            <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                <div class="col-md-8">
                    <input id="email" type="email" class="rounded-pill form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  autocomplete="email">

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            
            <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                <div class="col-md-8">
                    <input id="password" type="password" class="rounded-pill form-control @error('password') is-invalid @enderror" name="password"  autocomplete="new-password">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                <div class="col-md-8">
                    <input id="password-confirm" type="password" class="rounded-pill form-control" name="password_confirmation"  autocomplete="new-password">
                </div>
            </div>
            
            <div class="form-group row mb-0">
                <div class="col-md-8 offset-md-4">
                        <button type="submit" class="btn hover-btn btn-lg rounded-pill">
                            {{ __('Register') }}
                        </button>
    
                </div>
            </div>
        </form>
        
        
    </div>


    <div class="col-lg-6 col-12 d-flex justify-content-center align-items-center pt-5 pt-lg-0">
        <div class="hero-box w-100 d-flex justify-content-center align-items-center p-5">
          <img  class="h-100" src="/svg/HomeIllustration_2_v1.svg" alt="wireframe logo">
          {{-- <img  src="/svg/wireframeLogo.svg" alt="wireframe logo"> --}}
      </div>
       
    </div>
    
</div>








</div>
@endsection
