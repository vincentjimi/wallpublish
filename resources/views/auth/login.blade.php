@extends('layouts.app')

@section('content')
<div class="container" data-barba="container" data-barba-namespace="home">







    <div class="row flex-md-reverse">
        <div class="col-12">
            @if (Session::has('message_sent'))
                <div class="alert alert-success" role="alert">
                    {{ Session::get('message_sent') }}
                </div>
            @endif
        </div>
        <div class="form-col col-lg-6 col-12 text-secondary pb-5 pb-lg-0">
            <h1 class="py-4">Login</h1>
            @guest
                @if (Route::has('register'))
                    <div class="col-8 offset-3 d-flex flex-row-reverse"><small>
                        <div class="d-flex align-items-center flex-row">
                            Need an acount?</small> <strong><a class="text-right nav-link a pr-0" href="{{ route('register') }}">{{ __('Sign up') }}</a></strong>
                        </div>
                    </div>
                @endif
            @endguest
            
            <form method="POST" action="{{ route('login') }}">
                @csrf
                
                <div class="form-group row">

                    <label for="email" class="col-md-3 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                    <div class="col-md-8">
                        <input id="email" type="email" class="rounded-pill form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  autocomplete="email" autofocus>

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="password" class="col-md-3 col-form-label text-md-right">{{ __('Password') }}</label>

                    <div class="col-md-8">
                        <input id="password" type="password" class="rounded-pill form-control @error('password') is-invalid @enderror" name="password"  autocomplete="current-password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-8 offset-md-3">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                            <label class="form-check-label" for="remember">
                                {{ __('Remember Me') }}
                            </label>
                        </div>
                    </div>
                </div>
                
                <div class="form-group row mb-0">
                    <div class="col-md-8 offset-md-3">
                        <button type="submit" class="btn btn-lg hover-btn rounded-pill">
                            {{ __('Login') }}
                        </button>

                        @if (Route::has('password.request'))
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        @endif
                    </div>
                </div>
            </form>
            
            
        </div>

 
        <div class="col-lg-6 col-12 d-flex justify-content-center align-items-center pt-5 pt-lg-0">
            <div class="ninja-box w-100 d-flex justify-content-center align-items-center">
              <img  class="w-75" src="/svg/HomeIllustration_3_v1.png" alt="wireframe logo">
              {{-- <img  src="/svg/wireframeLogo.svg" alt="wireframe logo"> --}}
          </div>
           
        </div>
        
    </div>






</div>
@endsection
