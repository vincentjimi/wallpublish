@extends('layouts.app')

@section('content')
<div id="publication-create" class="container" data-barba="container" data-barba-namespace="publication-create">
    <div class="row px-4 px-md-0">
        <div class="col-12 d-flex justify-content-end ">
            <a class="btn btn-outline-danger rounded-circle" href="{{ url('/publication/'. $publication->id) }}"><div class="fa fa-times"></div></a>
        </div>
            <div class="col-12 text-secondary pb-3 pb-lg-0">
                <h1 class="py-4 orange">Edit publication</h1>
            </div>
            <form class="col-12 col-md-8 pb-4" method="POST" action="{{ url('/publication/'. $publication->id) }}" enctype="multipart/form-data">
                @csrf
                @method('PATCH')

                <div class="form-group row">
                    <label for="title" class="col-md-3 col-form-label text-md-right">{{ __('Title') }}</label>
                    <input id="title" type="text" maxlength="26" class="col-md-8 rounded-pill form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') ?? $publication->title }}"  autocomplete="name" autofocus>
                    
                    @error('title')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                
                <div class="form-group row">
                    <label for="description" class="col-md-3 col-form-label text-md-right">{{ __('Summary') }}</label>
                    <small class="d-block text-secondary text-center"></small>
                    <textarea style="border-radius:15px" rows="7" cols="30" maxlength="501" id="description" placeholder=" - Max 500 characters - " name="description" class="col-md-8 form-control @error('description') is-invalid @enderror" aria-label="With textarea">{{ old('description') ?? $publication->description }}</textarea>
                    
                    @error('description')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group row">
                    <label for="cover-image" class="col-md-3 col-form-label text-md-right">{{ __('Cover image') }}</label>
                <div class="custom-file col-md-8">
                    <input id="choose-file-edit" type="file" accept="image/*" class="custom-file-input @error('cover_image') is-invalid @enderror" id="cover-image" name="cover_image" value="{{ old('image') }}" aria-describedby="inputGroupFileAddon04">
                    <label for="cover-image" class="rounded-pill custom-file-label"></label>
                    @error('cover_image')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                    
                </div>    
            </div>    
            
            
            <div class="form-group row mb-0 px-0 mt-5">
                <div class="col-md-8 offset-md-3 px-0">
                    <button type="submit" class="btn hover-btn rounded-pill btn-lg">
                        {{ __('Save Publication') }}
                    </button> 
                </div>
            </div>   
        </form>  
        <div id="img-preview-edit" class="col-12 col-md-3 px-0"></div>





































    </div>
</div>    
@endsection
