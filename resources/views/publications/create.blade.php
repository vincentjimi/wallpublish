@extends('layouts.app')

@section('content')
<div id="publication-create" class="container" data-barba="container" data-barba-namespace="publication-create">
    <div class="row px-4 px-md-0">
            <div class="col-12 text-secondary pb-3 pb-lg-0">
                <h1 class="py-4 orange">Create a new publication</h1>
            </div>
            <form class="col-12 col-md-8 pb-4" method="POST" action="{{ url('/publication') }}" enctype="multipart/form-data">
                @csrf
                <div class="form-group row mb-4">
                    <label for="title" class="col-md-3 col-form-label text-md-right">{{ __('Title') }}</label>
                    <input id="title" type="text" placeholder="A new title" maxlength="26" class="col-md-8 rounded-pill form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}"  autocomplete="name" autofocus>
                    
                    @error('title')
                    <span class="invalid-feedback col-12 offset-3" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                
                <div class="form-group row mb-4">
                    <label for="description" class="col-md-3 col-form-label text-md-right">{{ __('Summary') }}</label>
                    <small class="d-block text-secondary text-center"></small>
                    <textarea style="border-radius:15px" rows="7" cols="30" maxlength="501" id="description" placeholder=" - Max 500 characters - " name="description" class="col-md-8 form-control @error('description') is-invalid @enderror" aria-label="With textarea">{{ old('description') }}</textarea>
                    
                    @error('description')
                    <span class="invalid-feedback col-12 offset-3" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group row mb-4 pb-2">
                    <label for="cover-image" class="col-md-3 col-form-label text-md-right">{{ __('Cover image') }}</label>
                <div class="custom-file col-md-8">
                    <input id="choose-file-cover-image" type="file" accept="image/*" class="custom-file-input @error('cover_image') is-invalid @enderror" name="cover_image" value="{{ old('image') }}" aria-describedby="inputGroupFileAddon04">
                    <label for="choose-file-cover-image" class="rounded-pill custom-file-label"></label>
                    @error('cover_image')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                    
                    </div>    
                </div>


                <div class="form-group row">
                    <label for="description" class="col-md-3 col-form-label text-md-right">{{ __('Select categories') }}</label>
                    
                    <fieldset class="col-md-8">
                        {{-- <legend>Select categories</legend> --}}
                        <div class="row p-4 border rounded switch-container">

                        <div class="col-md-6 d-flex py-2 align-items-center">
                            <input class="switch" type="checkbox" name="manga" value="1" id="manga">
                            <label class="pl-4 text-capitalize" for="manga">manga</label>
                        </div>
                        <div class="col-md-6 d-flex py-2 align-items-center">
                            <input class="switch" type="checkbox" name="comics" value="1" id="comics">
                            <label class="pl-4 text-capitalize" for="comics">comics</label>
                        </div>
                        <div class="col-md-6 d-flex py-2 align-items-center">
                            <input class="switch" type="checkbox" name="belgian_comics" value="1"  id="belgian-comics">
                            <label class="pl-4 text-capitalize" for="belgian-comics">belgian comics</label>
                        </div>
                        <div class="col-md-6 d-flex py-2 align-items-center">
                            <input class="switch" type="checkbox" name="comedy" value="1" id="comedy">
                            <label class="pl-4 text-capitalize" for="comedy">comedy</label>
                        </div>
                        <div class="col-md-6 d-flex py-2 align-items-center">
                            <input class="switch" type="checkbox" name="action" value="1" id="action">
                            <label class="pl-4 text-capitalize" for="action">action</label>
                        </div>
                        <div class="col-md-6 d-flex py-2 align-items-center">
                            <input class="switch" type="checkbox" name="adventure" value="1" id="adventure">
                            <label class="pl-4 text-capitalize" for="adventure">adventure</label>
                        </div>
                        <div class="col-md-6 d-flex py-2 align-items-center">
                            <input class="switch" type="checkbox" name="gore" value="1" id="gore">
                            <label class="pl-4 text-capitalize" for="gore">gore</label>
                        </div>
                        <div class="col-md-6 d-flex py-2 align-items-center">
                            <input class="switch" type="checkbox" name="fantasy" value="1" id="fantasy">
                            <label class="pl-4 text-capitalize" for="fantasy">fantasy</label>
                        </div>
                        <div class="col-md-6 d-flex py-2 align-items-center">
                            <input class="switch" type="checkbox" name="sci_fi" value="1"  id="sci-fi">
                            <label class="pl-4 text-capitalize" for="Sci">Sci-Fi</label>
                        </div>
                        <div class="col-md-6 d-flex py-2 align-items-center">
                            <input class="switch" type="checkbox" name="sport" value="1" id="sport">
                            <label class="pl-4 text-capitalize" for="sport">sport</label>
                        </div>
                        </div>
                    </fieldset>
                </div>
            
            
            
            <div class="form-group row mb-0 px-0 mt-5">
                <div class="col-md-8 offset-md-3 px-0">
                    <button type="submit" class="btn hover-btn rounded-pill btn-lg">
                        {{ __('Publish') }}
                    </button> 
                </div>
            </div>   
        </form>  
        <div id="img-preview" class="col-12 col-md-3 px-0"></div>





































    </div>
</div>    
@endsection
