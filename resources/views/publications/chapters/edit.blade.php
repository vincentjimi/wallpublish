@extends('layouts.app')

@section('content')


<div id="chapters" class="container pt-5" data-barba="container" data-barba-namespace="publication-show">
    
<div class="form-col row text-secondary pb-5 pb-lg-0 mb-5">
    <div class="col-12 d-flex justify-content-end ">
        <a class="btn btn-outline-danger rounded-circle" href="{{ url('publication/' . $publication->id . '/chapter/'.$chapter->id) }}"><div class="fa fa-times"></div></a>
    </div>
    <div class="col-12">

        <h1 class="py-4 orange"><small>edit</small>  {{ $chapter->chapter_name }}</h1>
    </div>
    <div class="col-6">

        <form method="POST" action="{{ url('publication/' . $publication->id . '/chapter/'.$chapter->id) }}">
            @csrf
            @method('PATCH')
            <div class="form-group row">
                <label for="chapter-name" class="col-md-3 col-form-label text-md-right">{{ __('Chapter name') }}</label>
                
            <div class="col-md-8">
                <input id="chapter-name" type="text" maxlength="26" class="rounded-pill form-control @error('chapter_name') is-invalid @enderror" name="chapter_name" value="{{ old('chapter_name') ?? $chapter->chapter_name }}" autocomplete="chapter_name" autofocus>
                
                @error('chapter_name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        
        <div class="form-group row mb-0">
            <div class="col-md-8 offset-md-3">
                <button type="submit" class="rounded-pill btn btn-lg bg-green text-white">
                    {{ __('Update') }}
                </button>
            </div>
        </div>
    </form>
</div>
</div>
    {{-- {{ dd($chapter->pages) }} --}}
    <div class="row pt-5">
        @forelse ( $chapter->pages as $page)
        <div class="col-md-3 col-sm-6 col-12 pb-4">
            <div class="del-edit d-flex justify-content-between">
                <div class="dropdown">
                        <a class="text-secondary" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">edit <i class="fa fa-edit"></i></a>
                      <div class="dropdown-menu p-2">

                        <form action="{{ url('/publication/'.$publication->id.'/chapter/'.$chapter->id.'/page/'.$page->id.'/update') }}" style="border-radius: 15px" class="dropzone dz-clickable py-5" id="my-awesome-dropzone" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="fallback">
                            <input name="page_path[]" type="file"  />
                            {{-- <input type="submit" value="Upload"> --}}
                        </div>
                        </form>
                    </div>
                              
                      </div>
                      <div class="dropdown">
                        <a class="text-danger" href="#" role="button" id="dropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          delete  <i class="fa fa-trash"></i>
                        </a>
                      
                        <div class="dropdown-menu p-2" aria-labelledby="dropdownMenuLink2">

                            <a class="btn btn-danger" href="{{ url('/publication/'.$publication->id.'/chapter/'.$chapter->id.'/page/'.$page->id.'/destroy') }}">Confirm delete</a>
                        </div>
                      </div>
            </div>
            <img class="w-100" src="{{ $page->page_path }}" alt="page">
        </div>
        @empty
        <p class="lead text-center w-100">There are no pages to edit in this chapter</p>
        @endforelse
    </div>

</div>

@endsection