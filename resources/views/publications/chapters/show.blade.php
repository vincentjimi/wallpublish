@extends('layouts.app')

@section('content')
<div id="chapter-show" class="container" data-barba="container" data-barba-namespace="chapter-show">

<div class="row">
  <div class="col-12">
    @if (isset(Auth::user()->id) && Auth::user()->id == $publication->user->id)
    <div class="py-5">
  
<p class="lead text-secondary">Drag and drop pages into the rectangle to add them to this chapter</p>
<form action="{{ url('publication/' . $publication->id . '/chapter/'.$chapter->id.'/add-pages') }}" style="border-radius: 15px" class="dropzone dz-clickable py-5" id="my-awesome-dropzone" method="POST" enctype="multipart/form-data">
  @csrf
  <div class="fallback">
    <input name="page_path[]" type="file" multiple />
    {{-- <input type="submit" value="Upload"> --}}
  </div>
</form>
</div>
@endif

  </div>
</div>
<div class="row border-bottom">
  <div class="col-12 d-flex justify-content-between">
    <h1>{{ $chapter->chapter_name }}</h1>
    @if (Auth::user() == $publication->user)
      
      <div class="del-edit d-flex flex-column text-right">
        <strong><a class="text-secondary del-edit" href="{{ url('/publication/'.$publication->id.'/chapter/'.$chapter->id.'/edit') }}">edit chapter <i class="fa fa-edit"></i></a></strong>
        <strong><a class="text-danger del-edit" href="{{ url('/publication/'.$publication->id.'/chapter/'.$chapter->id.'/destroy') }}">delete chapter <i class="fa fa-trash"></i></a></strong>
      </div>
    @endif
  </div>
  <div class="col-12 col-md-8 pb-4">
    
    <div class="btn-group w-100">
      <a class="btn hover-btn btn-lg dropdown-toggle rounded-pill" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Select another chapter
      </a>
    
      <div class="dropdown-menu chapter-dropdown-menu w-100" aria-labelledby="dropdownMenuLink">
        @foreach ($publication->chapters as $p)
        <a class="dropdown-item hover-orange" href="/publication/{{ $publication->id }}/chapter/{{ $p->id }}">{{ $p->chapter_name }}</a>
          @endforeach
      </div>
    </div>
    
  </div>
  <div class="col-12 col-md-4 bg-green-light py-3 mb-2">
    <div class="pl-md-4 pl-0">
      <a class="text-secondary text-decoration-none" href="{{ url('/publication/'.$publication->id) }}">
        <h5 class="">{{ $publication->title }}</h5>
      </a>
      <a class="text-secondary text-decoration-none" href="{{ url('/wall/'.$publication->user->id) }}">
      <h6 class="">by {{ $publication->user->name }}</h6> 
    </a>
      <a class="text-secondary text-decoration-none" href="{{ url('/publication/'.$publication->id) }}">
        <img class="publication-thumbnail" src="/storage/{{ $publication->cover_image }}" alt="cover_image">
      </a>
        <ul class="d-flex list-unstyled">
          <li class="pr-1"><i class="fa fa-eye"></i></li>12
          <li class="pl-3 pr-1"><i class="far fa-comment"></i></li>0
          <li class="pl-3 pr-1"><i class="far fa-bookmark"></i></li>{{ $publication->chapters->count() }} 
      </ul>
    </div>
    
  </div>   

   
</div>


<div class="row pt-5">
<div class="col-12">
  <!-- If we need pagination -->
   <div class="btn-container d-flex justify-content-between flex-row">
     <span id="slideLeft" class="btn-slide-right"></span>
     <div id="pagination-container" class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets">
       {{-- <span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0">1</span> --}}
       
      </div>
      <span id="slideRight" class="btn-slide-left"></span>
  </div>

  <!-- Slider main container -->
  <div class="swiper-container">
    <!-- Additional required wrapper -->
    <div class="swiper-wrapper py-5">
      <!-- Slides -->
      {{-- {{ dd($publication->chapters->pages) }} --}}
      {{-- {{ dd($chapter->pages) }} --}}
      @if ($chapter->pages)
        {{-- {{ dd($chapter->pages) }} --}}
        @forelse ( $chapter->pages as $pages)
          
        <div class="swiper-slide"><img src="{{ $pages->page_path }}" alt=""></div>
        @empty
          <p class="lead w-100 text-center">

            Oups ! Looks like this chapter does not have any pages yet.
          </p>
        @endforelse
      @else
        
      @endif

          
    
  </div>
 

  
  <!-- If we need navigation buttons -->
  <div class="swiper-button-prev"></div>
  <div class="swiper-button-next"></div>
  
  <!-- If we need scrollbar -->
  <div class="swiper-scrollbar"></div>
</div>
</div>

</div>
<div class="row mt-5 pt-5">
  <div class="col-12">
    @comments(['model' => $publication, 'perPage' => 2])
  </div>
</div>

</div>
@endsection
