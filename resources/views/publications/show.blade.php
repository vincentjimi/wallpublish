@extends('layouts.app')

@section('content')


<div id="chapters" class="container pt-5" data-barba="container" data-barba-namespace="publication-show">
    
    <div class="row publication-book-details pb-5 ">
        <div class="col-12 d-flex justify-content-between ">
            <h1>{{ $publication->title }}</h1>
            @can('update', $publication)
            <div class="d-flex flex-column">

                <a class="text-secondary text-right" href="{{ url('/publication/'.$publication->id.'/edit') }}">Edit publication <i class="fa fa-edit"></i></a>
                <div class="dropdown">
                    <a class="text-danger text-right" href="#" role="button" id="dropdownMenuLinkPublicationDestroy" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        delete publication <i class="fa fa-trash"></i>
                    </a>
                    
                    <div class="dropdown-menu p-2" aria-labelledby="dropdownMenuLinkPublicationDestroy">
                        
                        <a class="btn btn-danger" href="{{ url('/publication/'.$publication->id.'/destroy') }}">Confirm delete</a>
                    </div>
                </div>
            </div>
            @endcan
        </div>
        <div class="col-md-6 col-12">
            <img class="w-75" src="/storage/{{ $publication->cover_image }}" alt="cover_image">
            <div class="d-flex pb-4">
                <div class="d-flex align-items-center py-4">
                    <a class="text-secondary" href="/wall/{{ $publication->user->id }}"><img class="img-thumbnail rounded-circle" src="{{ $publication->user->wall->display_picture() }}" alt="img" width="50"></a>
                    <a class="text-secondary pl-4" href="/wall/{{ $publication->user->id }}"><h5><span class="font-weight-bold">{{ $publication->user->name }}</span></h5></a>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-12 d-flex flex-column justify-content-between">
            <div>

                <p class="text-secondary text-wrap text-break">{{ $publication->description }}</p>
                <ul class="d-flex list-unstyled">
                    <li class="px-1"><i class="fa fa-eye"></i></li>
                    <li class="px-1"><i class="far fa-comment"></i></li>
                    <li class="px-1"><i class="far fa-file"></i></li>
                </ul>
            </div>
            
            
    @can('update', $publication->user->wall)
        <div class="form-col text-secondary pb-5 pb-lg-0">
            <h4 class="py-4 orange">Add a new chapter</h4>
            
            <form method="POST" action="{{ url('publication/' . $publication->id . '/chapter') }}">
                @csrf
                <div class="form-group row">
                    <label for="chapter-name" class="col-md-3 col-form-label text-md-right">{{ __('Chapter name') }}</label>
                    
                    <div class="col-md-8">
                        <input id="chapter-name" type="text" maxlength="26" class="rounded-pill form-control @error('chapter_name') is-invalid @enderror" name="chapter_name" value="{{ old('chapter_name') }}" autocomplete="chapter_name" autofocus>
                        
                        @error('chapter_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                
               
                {{-- <div class="form-group row">
                    <label for="summary" class="col-md-3 col-form-label text-md-right">{{ __('summary') }}</label>
                    
                    <div class="col-md-8">
                        <small>Want to tell us more about this chapter?</small>
                        <textarea cols="30" rows="3" id="summary" style="border-radius:25px" class="form-control @error('summary') is-invalid @enderror" name="summary" value="{{ old('summary') }}" autocomplete="summary" autofocus></textarea>
                        
                        @error('summary')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $summary }}</strong>
                        </span>
                        @enderror
                    </div>
                </div> --}}
                
                
                
                <div class="form-group row mb-0">
                    <div class="col-md-8 offset-md-3">
                        <button type="submit" class="rounded-pill btn btn-lg bg-green text-white">
                            {{ __('Create') }}
                        </button>
                    </div>
                </div>
            </form>
        
        </div>
    @endcan
        </div>
    </div>

    
    <div class="row">
        <div class="col-12 px-0">
            
            <div class="accordion border-bottom mb-5" id="accordionExample">

                <div class="card">
                  <div class="card-header" id="headingTwo">
                    <h2 class="mb-0">
                      <button class="turn-caret-one btn btn-link btn-block text-left collapsed text-decoration-none" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        <h2 class="text-decoration-none text-secondary">Click here to see comments <i class="fas fa-caret-down caret-one"></i></h2>
                      </button>
                    </h2>
                  </div>
                  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body comments-card-body">
                        @comments(['model' => $publication, 'perPage' => 2])
                    </div>
                  </div>
                </div>

              </div>

        </div>
    </div>
    <div class="row">  
        @forelse ( $publication->chapters as $c)
            <div class="col-12 px-0">
                    <ul class="list-group w-100">
     
                        
                        <div class="list-group-item d-flex justify-content-between">
                            <a class="hover-orange" href="/publication/{{ $publication->id }}/chapter/{{ $c->id }}">
                                <h1 class="display-3 orange w-100 text-wrap hover-orange d-flex"><strong>{{ $c->chapter_name }}</strong></h1>
                            </a>
                            <div class="del-edit text-right">
                                <a class="text-secondary " href="{{ url('/publication/'.$publication->id.'/chapter/'.$c->id.'/edit') }}">edit <i class="fa fa-edit"></i></a>
                                <div class="dropdown">
                                    <a class="text-danger " href="#" role="button" id="dropdownMenuLinkChapterDelete" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        delete  <i class="fa fa-trash"></i>
                                    </a>
                                    
                                    <div class="dropdown-menu p-2" aria-labelledby="dropdownMenuLinkChapterDelete">
                                        <a class="btn btn-danger" href="{{ url('/publication/'.$publication->id.'/chapter/'.$c->id.'/destroy') }}">Confirm delete</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </ul>
            </div>
            
            @empty
            <div class="col-12">
                <p class="lead text-center">There are currently no chapters in this publication.</p>
            </div>
            
            @endforelse
          </div>
    
    

    
    

    </div>
@endsection
        