@extends('layouts.app')

@section('content')


<div id="publication-create" class="container" data-barba="container" data-barba-namespace="publication-create">
    <div class="row px-4 px-md-0">
        <div class="col-12 d-flex justify-content-end ">
            <a class="btn btn-outline-danger rounded-circle" href="{{ url('/wall/'.$user->id) }}"><div class="fa fa-times"></div></a>
        </div>
        <div class="col-12">
            <h1><small>Edit</small> Wall</h1>
        </div>
        <form class="col-12 col-md-8 pb-4" method="POST" action="/wall/{{ $user->id }}" enctype="multipart/form-data">
            @csrf
            @method('PATCH')

                    <div class="form-group row">
                        <label for="cover-image" class="col-md-3 col-form-label text-md-right">{{ __('Display picture') }}</label>
                    <div class="custom-file col-md-8">
                        <input id="choose-file-display-picture" type="file" accept="image/*" class="custom-file-input @error('display_picture') is-invalid @enderror" name="display_picture" value="{{ old('image') }}" aria-describedby="inputGroupFileAddon04">
                        <label for="choose-file-display-picture" class="rounded-pill custom-file-label"></label>
                        @error('display-picture')
                        <span class="invalid-feedback col-12 offset-3" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        
                    </div>    
                </div> 
                <div class="form-group row">
                    <label for="wall-title" class="col-md-3 col-form-label text-md-right">{{ __('Wall title') }}</label>
                    <input id="wall-title" type="text" maxlength="26" class="col-md-8 rounded-pill form-control @error('wall_title') is-invalid @enderror" name="wall_title" value="{{ old('wall_title') ?? $user->wall->wall_title }}"  autocomplete="name" autofocus>
                    
                    @error('wall_title')
                    <span class="invalid-feedback col-12 offset-3" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                
                <div class="form-group row">
                    <label for="bio" class="col-md-3 col-form-label text-md-right">{{ __('Tell us about yourself') }}</label>
                    <small class="d-block text-secondary text-center"></small>
                    <textarea style="border-radius:15px" rows="7" cols="30" maxlength="501" id="bio" placeholder=" - Max 500 characters - " name="bio" class="col-md-8 form-control @error('bio') is-invalid @enderror" aria-label="With textarea">{{ old('bio') ?? $user->wall->bio }}</textarea>
                    
                    @error('bio')
                    <span class="invalid-feedback col-12 offset-3" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
             
            
            <div class="form-group row">
                <label for="description" class="col-md-3 col-form-label text-md-right">{{ __('Select categories') }}</label>
                
                <fieldset class="col-md-8">
                    {{-- <legend>Select categories</legend> --}}
                    <div class="row p-4 border rounded switch-container">
    
                    <div class="col-lg-6 col-12 d-flex py-2 align-items-center">
                        <input class="switch" type="checkbox" name="manga" value="1" id="manga" {{  ($user->wall->manga == 1 ? 'checked' : '') }}>
                        <label class="pl-4 text-capitalize" for="manga">manga</label>
                    </div>
                    <div class="col-lg-6 col-12 d-flex py-2 align-items-center">
                        <input class="switch" type="checkbox" name="comics" value="1" id="comics" {{  ($user->wall->comics == 1 ? 'checked' : '') }}>
                        <label class="pl-4 text-capitalize" for="comics">comics</label>
                    </div>
                    <div class="col-lg-6 col-12 d-flex py-2 align-items-center">
                        <input class="switch" type="checkbox" name="belgian_comics" value="1"  id="belgian-comics" {{  ($user->wall->belgian_comics == 1 ? 'checked' : '') }}>
                        <label class="pl-4 text-capitalize" for="belgian-comics">belgian comics</label>
                    </div>
                    <div class="col-lg-6 col-12 d-flex py-2 align-items-center">
                        <input class="switch" type="checkbox" name="comedy" value="1" id="comedy" {{  ($user->wall->comedy == 1 ? 'checked' : '') }}>
                        <label class="pl-4 text-capitalize" for="comedy">comedy</label>
                    </div>
                    <div class="col-lg-6 col-12 d-flex py-2 align-items-center">
                        <input class="switch" type="checkbox" name="action" value="1" id="action" {{  ($user->wall->action == 1 ? 'checked' : '') }}>
                        <label class="pl-4 text-capitalize" for="action">action</label>
                    </div>
                    <div class="col-lg-6 col-12 d-flex py-2 align-items-center">
                        <input class="switch" type="checkbox" name="adventure" value="1" id="adventure" {{  ($user->wall->adventure == 1 ? 'checked' : '') }}>
                        <label class="pl-4 text-capitalize" for="adventure">adventure</label>
                    </div>
                    <div class="col-lg-6 col-12 d-flex py-2 align-items-center">
                        <input class="switch" type="checkbox" name="gore" value="1" id="gore" {{  ($user->wall->gore == 1 ? 'checked' : '') }}>
                        <label class="pl-4 text-capitalize" for="gore">gore</label>
                    </div>
                    <div class="col-lg-6 col-12 d-flex py-2 align-items-center">
                        <input class="switch" type="checkbox" name="fantasy" value="1" id="fantasy" {{  ($user->wall->fantasy == 1 ? 'checked' : '') }}>
                        <label class="pl-4 text-capitalize" for="fantasy">fantasy</label>
                    </div>
                    <div class="col-lg-6 col-12 d-flex py-2 align-items-center">
                        <input class="switch" type="checkbox" name="sci_fi" value="1"  id="sci-fi" {{  ($user->wall->sci_fi == 1 ? 'checked' : '') }}>
                        <label class="pl-4 text-capitalize" for="Sci">Sci-Fi</label>
                    </div>
                    <div class="col-lg-6 col-12 d-flex py-2 align-items-center">
                        <input class="switch" type="checkbox" name="sport" value="1" id="sport" {{  ($user->wall->sport == 1 ? 'checked' : '') }}>
                        <label class="pl-4 text-capitalize" for="sport">sport</label>
                    </div>
                    </div>
                </fieldset>
            </div>
            
            
            
            <div class="form-group row mb-0 px-0 mt-5">
                <div class="col-md-8 offset-md-3 px-0">
                    <button type="submit" class="btn hover-btn rounded-pill btn-lg">
                        {{ __('Save profile') }}
                    </button> 
                </div>
            </div>   
        </form>  
        <div id="img-preview-display-picture" class="col-12 col-md-3 px-0 d-flex justify-content-center align-items-start">
            <div class="dp-container d-flex justify-content-center">
                <img class="img-thumbnail rounded-circle" width="400" src="{{ $user->wall->display_picture() }}" alt="img">
            </div>
        </div>


    </div>
</div>   
@endsection
