@extends('layouts.app')

@section('content')
<div class="container" data-barba="container" data-barba-namespace="wall-index">
    <div class="row border-bottom">
        <div class="col-12 col-md-8 offset-md-2 d-flex flex-column justify-content-center align-items-center" >
            <div class="dp-container d-flex justify-content-center">
                <img class="img-thumbnail rounded-circle" width="200" src="{{ $user->wall->display_picture() }}" alt="img">
            </div>
            <div class="pb-5 pt-3">
                <div class="d-flex">
                    <h3 class="pr-3">{{ $user->name }}</h3>
                    <follow-button user-id="{{ $user->id }}" follows="{{$follows}}"></follow-button>
                </div>
                @can('update', $user->wall)
                <a class="text-secondary" href="/wall/{{ $user->id }}/edit">Edit profile <i class="fas fa-pen"></i></a>
                @endcan
            </div>
        </div>
        <div class="col-12 col-md-6 offset-md-3 d-flex justify-content-between pb-5">
            <div class=""><h5><strong>{{ $publicationCount }}</strong> Publishes</h5></div>
            <div class=""><h5><strong>{{ $followingCount }}</strong> following</h5></div>
            <div class=""><h5><strong>{{ $followersCount }}</strong> followers</h5></div>
        </div>
        <div class="col-12">
            <h1>{{ $user->wall->wall_title }}</h1>
            <p class="text-left lead text-secondary">{{ $user->wall->bio }}</p>
        </div>
    </div>
    <div class="row px-1">
        @foreach ($user->publications as $publication)
        <div class="col-md-4 col-lg-3 col-sm-6 col-12 p-3 pt-5">
            <div class="publication-card">
                <div class="img-container">
                    <img class="card-img-top" src="/storage/{{ $publication->cover_image }}" alt="Card image cap">          
                </div>
                <div class="card-details">
                    <h4 class="card-title">{{ $publication->title }}</h4>
                    <div class="p-container">
                        <p class="card-text text-wrap text-break">{{ $publication->description }}</p>
                    </div>
                    <div class="d-flex justify-content-center pt-2">
                        <a href="/publication/{{ $publication->id }}" class="btn btn-outline-secondary rounded-pill">Read now</a>
                    </div>
                </div>
            </div>
            <div class="d-flex align-items-top pt-2">
                <a class="text-secondary" href="/wall/{{ $publication->user->id }}"><img class="rounded-circle img-thumbnail mr-2" src="{{ $publication->user->wall->display_picture() }}" style=" width:50px; height:50px;" alt="DP"></a>
                <div>
                    <a href="/publication/{{ $publication->id }}" class="text-decoration-none"><h6 class="card-title orange mb-0"><strong>{{ $publication->title }}</strong></h6></a>
                    
                    <a class="text-secondary" href="/wall/{{ $publication->user->id }}"><h6>{{ $publication->user->name }}</h6></a>
                    <ul class="d-flex list-unstyled">
                        <li class="pr-1"><i class="fa fa-eye"></i></li>12
                        <li class="pl-3 pr-1"><i class="far fa-comment"></i></li>0
                        <li class="pl-3 pr-1"><i class="far fa-file"></i></li>{{ $publication->chapters->count() }}
                        
                    </ul>
                </div>
            </div>
        </div>
        @endforeach
        
    </div>
</div>
@endsection
