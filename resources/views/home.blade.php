@extends('layouts.app')

@section('content')
<div id="home" class="container" data-barba="container" data-barba-namespace="home">
<div class="row px-1">
    
    @forelse ( $interested as $intrest)
    <div class="col-12">
        <h2 class="border-bottom">
            Publications that may interest you
        </h2>
    </div>
    <div class="col-md-4 col-lg-3 col-sm-6 col-12 p-3 pt-5">
        <div class="publication-card">
            <div class="img-container">
                <img class="card-img-top" src="/storage/{{ $intrest->cover_image }}" alt="Card image cap">          
            </div>
            <div class="card-details">
                <h4 class="card-title">{{ $intrest->title }}</h4>
                <div class="p-container">
                    <p class="card-text text-break text-wrap">{{ $intrest->description }}</p>
                </div>
                <div class="d-flex justify-content-center pt-2">
                    <a href="/publication/{{ $intrest->id }}" class="btn btn-outline-secondary rounded-pill">Read now</a>
                </div>
            </div>
        </div>
        <div class="d-flex align-items-top pt-2">
            <a class="text-secondary" href="/wall/{{ $intrest->user->id }}"><img class="rounded-circle img-thumbnail mr-2" src="{{ $intrest->user->wall->display_picture() }}" style=" width:50px; height:50px;" alt="DP"></a>
            <div>
                <a href="/publication/{{ $intrest->id }}" class="text-decoration-none"><h6 class="card-title orange mb-0"><strong>{{ $intrest->title }}</strong></h6></a>
                
                <a class="text-secondary" href="/wall/{{ $intrest->user->id }}"><h6>{{ $intrest->user->name }}</h6></a>
                <ul class="d-flex list-unstyled">
                    <li class="pr-1"><i class="fa fa-eye"></i></li>12
                    <li class="pl-3 pr-1"><i class="far fa-comment"></i></li>0
                    <li class="pl-3 pr-1"><i class="far fa-file"></i></li>{{ $intrest->chapters->count() }}
                    
                </ul>
            </div>
        </div>
    </div>


    @empty

    <div class="col-12">
        <h1>Finish setting up your profile</h1>
    </div>
    <form class="col-12 col-md-8 pb-4" method="POST" action="/wall/{{ Auth::user()->id }}" enctype="multipart/form-data">
        @csrf
        @method('PATCH')

        <div class="form-group row mb-4 pb-2">
            <label for="cover-image" class="col-md-3 col-form-label text-md-right">{{ __('Display picture') }}</label>
        <div class="custom-file col-md-8">
            <input id="choose-file-display-picture" type="file" accept="image/*" class="custom-file-input @error('display_picture') is-invalid @enderror" name="display_picture" value="{{ old('image') }}" aria-describedby="inputGroupFileAddon04">
            <label for="choose-file-display-picture" class="rounded-pill custom-file-label"></label>
            @error('display-picture')
            <span class="invalid-feedback col-12 offset-3" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
            
            </div>    
        </div>
            <div class="form-group row mb-4">
                <label for="wall-title" class="col-md-3 col-form-label text-md-right">{{ __('Wall title') }}</label>
                <input id="wall-title" type="text" maxlength="26" class="col-md-8 rounded-pill form-control @error('wall_title') is-invalid @enderror" name="wall_title" value="{{ old('wall_title') ?? Auth::user()->wall->wall_title }}"  autocomplete="name" autofocus>
                
                @error('wall_title')
                <span class="invalid-feedback col-12 offset-3" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            
            <div class="form-group row mb-4">
                <label for="bio" class="col-md-3 col-form-label text-md-right">{{ __('Tell us about yourself') }}</label>
                <small class="d-block text-secondary text-center"></small>
                <textarea style="border-radius:15px" rows="7" cols="30" maxlength="501" id="bio" placeholder=" - Max 500 characters - " name="bio" class="col-md-8 form-control @error('bio') is-invalid @enderror" aria-label="With textarea">{{ old('bio') ?? Auth::user()->wall->bio }}</textarea>
                
                @error('bio')
                <span class="invalid-feedback col-12 offset-3" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
    
        <div class="form-group row">
            <label for="description" class="col-md-3 col-form-label text-md-right">{{ __('Select categories') }}</label>
            
            <fieldset class="col-md-8">
                {{-- <legend>Select categories</legend> --}}
                <div class="row p-4 border rounded switch-container">

                <div class="col-lg-6 col-12 d-flex py-2 align-items-center">
                    <input class="switch" type="checkbox" name="manga" value="1" id="manga">
                    <label class="pl-4 text-capitalize" for="manga">manga</label>
                </div>
                <div class="col-lg-6 col-12 d-flex py-2 align-items-center">
                    <input class="switch" type="checkbox" name="comics" value="1" id="comics">
                    <label class="pl-4 text-capitalize" for="comics">comics</label>
                </div>
                <div class="col-lg-6 col-12 d-flex py-2 align-items-center">
                    <input class="switch" type="checkbox" name="belgian_comics" value="1"  id="belgian-comics">
                    <label class="pl-4 text-capitalize" for="belgian-comics">belgian comics</label>
                </div>
                <div class="col-lg-6 col-12 d-flex py-2 align-items-center">
                    <input class="switch" type="checkbox" name="comedy" value="1" id="comedy">
                    <label class="pl-4 text-capitalize" for="comedy">comedy</label>
                </div>
                <div class="col-lg-6 col-12 d-flex py-2 align-items-center">
                    <input class="switch" type="checkbox" name="action" value="1" id="action">
                    <label class="pl-4 text-capitalize" for="action">action</label>
                </div>
                <div class="col-lg-6 col-12 d-flex py-2 align-items-center">
                    <input class="switch" type="checkbox" name="adventure" value="1" id="adventure">
                    <label class="pl-4 text-capitalize" for="adventure">adventure</label>
                </div>
                <div class="col-lg-6 col-12 d-flex py-2 align-items-center">
                    <input class="switch" type="checkbox" name="gore" value="1" id="gore">
                    <label class="pl-4 text-capitalize" for="gore">gore</label>
                </div>
                <div class="col-lg-6 col-12 d-flex py-2 align-items-center">
                    <input class="switch" type="checkbox" name="fantasy" value="1" id="fantasy">
                    <label class="pl-4 text-capitalize" for="fantasy">fantasy</label>
                </div>
                <div class="col-lg-6 col-12 d-flex py-2 align-items-center">
                    <input class="switch" type="checkbox" name="sci_fi" value="1"  id="sci-fi">
                    <label class="pl-4 text-capitalize" for="Sci">Sci-Fi</label>
                </div>
                <div class="col-lg-6 col-12 d-flex py-2 align-items-center">
                    <input class="switch" type="checkbox" name="sport" value="1" id="sport">
                    <label class="pl-4 text-capitalize" for="sport">sport</label>
                </div>
                </div>
            </fieldset>
        </div>
        
        <div class="form-group row mb-0 px-0 mt-5">
            <div class="col-md-8 offset-md-3 px-0">
                <button type="submit" class="btn hover-btn rounded-pill btn-lg">
                    {{ __('Save profile') }}
                </button> 
            </div>
        </div>   
    </form>  
    <div id="img-preview-display-picture" class="col-12 col-md-3 px-0 d-flex justify-content-center align-items-start">
        <div class="dp-container d-flex justify-content-center">
            <img class="img-thumbnail rounded-circle" width="400" src="{{ Auth::user()->wall->display_picture() }}" alt="img">
        </div>
    </div>
        
    @endforelse
</div>


{{-- ------------------------------------- Authors I follow ------------------------------------------------------------------------------------------------- --}}
<div class="row px-1">
    @if ($interested->isNotEmpty())
    <div class="col-12">
        <h2 class="border-bottom">
            Authors you follow
        </h2>
    </div>
    
    @foreach ($publications as $publication)
        
    <div class="col-md-4 col-lg-3 col-sm-6 col-12 p-3 pt-5">
        <div class="publication-card">
            <div class="img-container">
                <img class="card-img-top" src="/storage/{{ $publication->cover_image }}" alt="Card image cap">          
            </div>
            <div class="card-details">
                <h4 class="card-title">{{ $publication->title }}</h4>
                <div class="p-container">
                    <p class="card-text text-break text-wrap">{{ $publication->description }}</p>
                </div>
                <div class="d-flex justify-content-center pt-2">
                    <a href="/publication/{{ $publication->id }}" class="btn btn-outline-secondary rounded-pill">Read now</a>
                </div>
            </div>
        </div>
        <div class="d-flex align-items-top pt-2">
            <a class="text-secondary" href="/wall/{{ $publication->user->id }}"><img class="rounded-circle img-thumbnail mr-2" src="{{ $publication->user->wall->display_picture() }}" style=" width:50px; height:50px;" alt="DP"></a>
            <div>
                <a href="/publication/{{ $publication->id }}" class="text-decoration-none"><h6 class="card-title orange mb-0"><strong>{{ $publication->title }}</strong></h6></a>
                
                <a class="text-secondary" href="/wall/{{ $publication->user->id }}"><h6>{{ $publication->user->name }}</h6></a>
                <ul class="d-flex list-unstyled">
                    <li class="pr-1"><i class="fa fa-eye"></i></li>12
                    <li class="pl-3 pr-1"><i class="far fa-comment"></i></li>0
                    <li class="pl-3 pr-1"><i class="far fa-file"></i></li>{{ $publication->chapters->count() }}
                    
                </ul>
            </div>
        </div>
    </div>
    
    @endforeach
    @endif


</div>
</div>
@endsection
