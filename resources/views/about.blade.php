@extends('layouts.app')

@section('content')
<div id="about" class="container" data-barba="container" data-barba-namespace="about">
    <div class="row flex-md-reverse">
        <div class="col-lg-6 col-12 text-secondary py-5">
            <h1 class="py-4">About</h1>
            <p>
                Hi there, I'm Vincent Ogunjimi a graphics designer and full-stack web developer with an engineering background. I am a big fan of anime and mangas, and I also write my own comic books. 
            </p>

            <p>
                One of the major challenges I used to have, was finding a way to share my works with people who might be interested in it. For a while I tried using social media but they did not really seem to be 
                adapted to this type of content, with that said, I know that there are websites made for posting all forms of graphic novels, but most of them are full of adverts, something I particularly hate and find distracting.
            </p>
            <p>
                So I decided to create my own platform with the user experience being my primary goal, I consider that the website which serves as a reading platform is just as important as the book itself being read.
            </p>
            <p>
                Initially this website was suppose to exist only in order to make sharing my work easier, but I thought I shouldn't limit this to my self, so I decided to make it a platform that's accessible to everyone, 
                everywhere and completely free. I hope that together we can make it stand out and benefit as much people as it can.
            </p>
                
                
                
        </div>
        <div class="col-lg-6 col-12 d-flex justify-content-center align-items-center py-5">
            <div class="box">
                <span style="--i:1"> <img  src="/svg/wireframeLogo.svg" alt="wireframe logo"> </span>
                <span style="--i:2"> <img  src="/svg/wireframeLogo.svg" alt="wireframe logo"> </span>
                <span style="--i:3"> <img  src="/svg/wireframeLogo.svg" alt="wireframe logo"> </span>
                <span style="--i:4"> <img  src="/svg/wireframeLogo.svg" alt="wireframe logo"> </span>
                <span> <img style="transform: scale(1.2) translateY(-30px)" src="/svg/wallpublish_logo_final.svg" alt="wireframe logo"> </span>
                <span style="--i:5"> <img  src="/svg/wireframeLogo.svg" alt="wireframe logo"> </span>
                <span style="--i:6"> <img  src="/svg/wireframeLogo.svg" alt="wireframe logo"> </span>
                <span style="--i:7"> <img  src="/svg/wireframeLogo.svg" alt="wireframe logo"> </span>
                <span style="--i:8"> <img  src="/svg/wireframeLogo.svg" alt="wireframe logo"> </span>
                <span style="--i:9"> <img  src="/svg/wireframeLogo.svg" alt="wireframe logo"> </span>
              </div>
        </div>

    </div>
</div>
@endsection
