<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="https://kit.fontawesome.com/39f0c3e5ce.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.2/min/dropzone.min.js"
    integrity="sha512-VQQXLthlZQO00P+uEu4mJ4G4OAgqTtKG1hri56kQY1DtdLeIqhKUp9W/lllDDu3uN3SnUNawpW7lBda8+dSi7w=="
    crossorigin="anonymous"></script>
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@barba/core"></script>
    <script src="https://unpkg.com/gsap@latest/dist/gsap.min.js"></script>
    {{-- <script src="https://cdn.jsdelivr.net/npm/masonry-layout@4.2.2/dist/masonry.pkgd.min.js" integrity="sha384-GNFwBvfVxBkLMJpYMOABq3c+d3KnQxudP/mGPkzpZSTYykLBNsZEnG2D9G/X/+7D" crossorigin="anonymous" async></script> --}}
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.2/dropzone.min.css"
        integrity="sha512-jU/7UFiaW5UBGODEopEqnbIAHOI8fO6T99m7Tsmqs2gkdujByJfkCbbfPSN4Wlqlb9TGnsuC0YgUgWkRBK7B9A=="
        crossorigin="anonymous" />
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    
</head>
<body data-barba="wrapper">
    <div id="app" class="mt-5 pt-5">
        <nav id="nav-active" class="navbar navbar-expand-md navbar-ligt bg-white shadow-sm fixed-top">
            <div class="container-fluid">
                <a class="navbar-brand logo-text text-danger mr-1" href="{{ url('/') }}">
                    <img src="/svg/wallpublish_logo_final.svg" width="40" alt="Logo">
                    <div class="d-none d-lg-inline px-1 logo-text">Wall<span class="green">publish</span></div>
                    {{-- <div class="d-none d-md-inline px-1 logo-text">{{ config('app.name', 'Wallpublish') }}</div> --}}
                </a>
                <button class="navbar-toggler mr-n2" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    {{-- <span class="navbar-toggler-icon"></span> --}}
                            <!-- hambuger manu icon  -->
        <svg class="ham hamRotate ham1" viewBox="0 0 100 100" width="55" onclick="this.classList.toggle('active')">
            <path class="line top"
              d="m 30,33 h 40 c 0,0 9.044436,-0.654587 9.044436,-8.508902 0,-7.854315 -8.024349,-11.958003 -14.89975,-10.85914 -6.875401,1.098863 -13.637059,4.171617 -13.637059,16.368042 v 40" />
            <path class="line middle" d="m 30,50 h 40" />
            <path class="line bottom"
              d="m 30,67 h 40 c 12.796276,0 15.357889,-11.717785 15.357889,-26.851538 0,-15.133752 -4.786586,-27.274118 -16.667516,-27.274118 -11.88093,0 -18.499247,6.994427 -18.435284,17.125656 l 0.252538,40" />
  
                </button>
                
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto d-flex align-items-md-center">
                        <search-bar></search-bar>
                        <li class="ml-md-4  nav-item  d-flex align-items-center home">
                            <a class="nav-link hover-orange green " href="{{ url('/home') }}">Home<span class="sr-only">(current)</span></a>
                          </li>
                          <li class="mx-md-1 nav-item d-flex align-items-center discover">
                            <a class="nav-link hover-orange green " href="{{ url('/') }}">Discover</a>
                          </li>
                          <li class="mx-md-1 nav-item d-flex align-items-center about">
                            <a class="nav-link hover-orange green " href="{{ url('/about') }}">About</a>
                          </li>
                          <li class="mx-md-1 nav-item d-flex align-items-center contact">
                            <a class="nav-link hover-orange green " href="{{ url('/contact') }}">Contact</a>
                          </li>
                          
                          <!-- Authentication Links -->
                          @guest
                          @if (Route::has('login'))
                          <li class="ml-md-2 nav-item d-flex align-items-center pt-md-0 pt-3 pl-md-4">
                              <strong><a class="nav-link a" href="{{ route('login') }}">{{ __('Login') }}</a></strong>
                                </li>
                                @endif
                            
                        @else
                        <li class="ml-md-2 nav-item ">
                            <div class="btn-group d-flex align-items-md-center">
                                
                                <a id="navbarDropdown" class="nav-link p-1" href="/wall/{{ Auth::user()->id }}">
                                    <img class="rounded-circle" src="{{ Auth::user()->wall->display_picture() }}" width="30" alt="">
                                </a>
                                <a role="button" class="p-1 a dropdown-toggle dropdown-toggle-split" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </a>
                                
                                <div class="dropdown-menu dropdown-menu-right shadow border-0" aria-labelledby="navbarDropdown" style="border-radius:15px">
                                    <div class="dropdown-header d-flex align-items-center" style="width: 200px;">
                                        <div class="pr-2"><img class="rounded-circle" src="{{ Auth::user()->wall->display_picture() }}" width="40" alt=""></div>
                                        <div class=""><h6 class="mb-0">{{ Auth::user()->name }}</h6></div>
                                    </div>
                                    
                                    <hr class="dropdown-divider">
                                    <a class="dropdown-item" href="/wall/{{ Auth::user()->id }}">My profile</a>
                                    <a class="dropdown-item" href="/wall/{{ Auth::user()->id }}/edit">Settings</a>
                                    <hr class="dropdown-divider">
                                    <a class="dropdown-item" href="#"
                                    onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </div>
                            </li>
                            @endguest
                    </ul>
                </div>
            </div>
        </nav>
        
        <div class="container-fluid">
            <div class="row">
                <div class="col-1 offset-11">
                    <div class="footer-icon-container">
                        @if (Auth::user())
                            
                        <div class="pb-3">
                            <a class="bg-white btn shadow d-flex justify-content-center align-items-center footer-btn rounded-circle" href="{{ url('/publication/create') }}" >
                                <i class="fas fa-plus"></i>
                            </a>
                        </div>
                        @endif

                        <div>
                            <a href="#" class="bg-white btn shadow d-flex justify-content-center align-items-center footer-btn rounded-circle" type="button" data-toggle="collapse" data-target="#navbarToggleFooter" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
                                <i class="fas fa-question"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fixed-bottom">
                <div class="collapse" id="navbarToggleFooter">
                    <div class="bg-green p-4">
                        <div class="col-12 d-flex justify-content-end">
                            <a href="#" class="bg-white btn rounded-circle green a" type="button" data-toggle="collapse" data-target="#navbarToggleFooter" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
                                <i class="fas fa-times"></i>
                            </a>
                        </div>

                                <!-- footer and resources -->
                                <footer class="container">
                                    <div class="row border-bottom">
                                        <div class="col-md-6 pb-md-4 pr-5 col-12 px-0 text-light">
                                            <h3>Wallpublish</h3>
                                            <p class="pr-md-5">Wallpublish is a platform for sharing comics/mangas, it is completely free and accessible by anyone who loves to read or write mangas/comics, please feel free to post your books here  together we can build a large community !</p>  
                                        </div>

                                        <div class="col-md-6 pb-md-4 col-12 pl-0 text-light">
                                            <h3 class="">Resources</h3>
                                            <div class="d-flex flex-column">
        
                                                <ul class="list-group d-flex flex-row">
                                                    <li class="list-group-item border-0 bg-green pl-0">
                                                        <a class="text-decoration-none text-light" href="#">Support</a>
                                                    </li>
                                                    <li class="list-group-item border-0 bg-green pl-0">
                                                        <a class="text-decoration-none text-light" href="#">FAQS</a>
                                                    </li>
                                                </ul>
                                                
                                                <ul class="d-flex flex-row list-group">
                                                    <li class="hover-link list-group-item border-0 bg-green text-white pl-0">
                                                        <a class="text-decoration-none text-light" href="#"><i class="fab fa-facebook-f"></i></a>
                                                    </li>
                                                    <li class="hover-link list-group-item border-0 bg-green text-white">
                                                        <a class="text-decoration-none text-light" href="#"><i class="fab fa-instagram"></i></a>
                                                    </li>
                                                    <li class="hover-link list-group-item border-0 bg-green text-white">
                                                        <a class="text-decoration-none text-light" href="#"><i class="fab fa-twitter"></i></a>
                                                    </li>
                                                    <li class="hover-link list-group-item border-0 bg-green text-white">
                                                        <a class="text-decoration-none text-light" href="#"><i class="fab fa-linkedin-in"></i></a>
                                                    </li>
                                                </ul>
        
                                            </div>
                                        </div>
                                    </div>
                                <!-- End of top row  -->

                                    <!-- Bottom row  -->
                                    <div class="row text-light pt-2">
                                        <div class="col-md-6 col-12 pl-0">© 2021 U Coach Me. All Rights Reserved.</div>
                                        <div class="col-md-6 col-12 text-md-right px-0"><a class="pl-0 pr-2 nav-link d-md-inline text-white" href="#">Privacy Policy</a><a class="pl-0 nav-link d-md-inline text-white pr-0" href="#">Terms of service</a></div>
                                    </div>
                                    <!-- End of bottom row  -->
                                </footer>
                          </div>
                        </div>
                    </div>
    
        </div>
        {{-- <div class="wireframe-logo @if(Request::is('/')) d-none @endif">
            <img  src="/svg/wireframeLogo.svg" alt="wireframe logo">
        </div> --}}

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
