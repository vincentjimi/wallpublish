@extends('layouts.app')

@section('content')
<div id="contact" class="container" data-barba="container" data-barba-namespace="contact">
    <div class="row flex-md-reverse">
        <div class="col-12">
            @if (Session::has('message_sent'))
                <div class="alert alert-success" role="alert">
                    {{ Session::get('message_sent') }}
                </div>
            @endif
        </div>
        <div class="form-col col-lg-6 col-12 text-secondary pb-5 pb-lg-0">
            <h1 class="py-4">Contact Us</h1>
            
            <form method="GET" action="{{ route('contact.send') }}">
                @csrf
                
                <div class="form-group row">
                    <label for="name" class="col-md-3 col-form-label text-md-right">{{ __('Name') }}</label>
                    
                    <div class="col-md-8">
                        <input id="name" type="text" class="rounded-pill form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="email" class="col-md-3 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                    
                    <div class="col-md-8">
                        <input id="email" type="email" class="rounded-pill form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                        
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="subject" class="col-md-3 col-form-label text-md-right">{{ __('Subject') }}</label>
                    
                    <div class="col-md-8">
                        <input id="subject" type="text" class="rounded-pill form-control @error('subject') is-invalid @enderror" name="subject" value="{{ old('subject') }}" required autocomplete="subject" autofocus>
                        
                        @error('subject')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="message" class="col-md-3 col-form-label text-md-right">{{ __('Message') }}</label>
                    
                    <div class="col-md-8">
                        <textarea cols="30" rows="10" id="message" style="border-radius:25px" class="form-control @error('message') is-invalid @enderror" name="message" value="{{ old('message') }}" required autocomplete="message" autofocus></textarea>
                        
                        @error('message')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                
                
                
                <div class="form-group row mb-0">
                    <div class="col-md-8 offset-md-3">
                        <button type="submit" class="rounded-pill btn btn-lg bg-green hover-btn text-white">
                            {{ __('Send') }}
                        </button>
                    </div>
                </div>
            </form>
            
            
        </div>

        <div class="col-lg-6 col-12 d-flex justify-content-center align-items-center pt-5 pt-lg-0">
            <div class="box">
                <span style="--i:1"> <img  src="/svg/wireframeLogo.svg" alt="wireframe logo"> </span>
                <span style="--i:2"> <img  src="/svg/wireframeLogo.svg" alt="wireframe logo"> </span>
                <span style="--i:3"> <img  src="/svg/wireframeLogo.svg" alt="wireframe logo"> </span>
                <span style="--i:4"> <img  src="/svg/wireframeLogo.svg" alt="wireframe logo"> </span>
                <span> <img style="transform: scale(1.2) translateY(-30px)" src="/svg/wallpublish_logo_final.svg" alt="wireframe logo"> </span>
                <span style="--i:5"> <img  src="/svg/wireframeLogo.svg" alt="wireframe logo"> </span>
                <span style="--i:6"> <img  src="/svg/wireframeLogo.svg" alt="wireframe logo"> </span>
                <span style="--i:7"> <img  src="/svg/wireframeLogo.svg" alt="wireframe logo"> </span>
                <span style="--i:8"> <img  src="/svg/wireframeLogo.svg" alt="wireframe logo"> </span>
                <span style="--i:9"> <img  src="/svg/wireframeLogo.svg" alt="wireframe logo"> </span>
            </div>
        </div>
        
    </div>
</div>
@endsection
