@extends('layouts.app')

@section('content')
    <div class="container-fluid welcome-container" data-barba="container" data-barba-namespace="welcome">
    <div id="discover" class="container">

        <div class="background">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
    
        </div>
      



      <div class="row welcome pt-md-5 mt-md-5 px-0 flex-md-row-reverse">
        <div class="col-md-6 col-12">
            <div class="welcome-illustrations d-flex flex-column align-items-center pb-4">
                <img src="/svg/welcomeIllustration.svg" alt="">
            </div>
        </div>
        <div class="col-md-6 col-12">
            <h1 class="orange pb-5">Discover new talents in graphic
                novels with Wall<span class="green">publish</span></h1>
            <p class="text-secondary lead">
                This is a platform for sharing comics/mangas, it is completely free and accessible by anyone who loves to read or write mangas/comics, please feel free to post your books here  together we can build a large community ! 
            </p>
            <strong>
                <a href="{{ url('/about') }}" class="green link-item hover-orange">Click here to learn more about Wallpublish</a>
            </strong>
        </div>
        <div class="d-flex justify-content-center w-100 py-4">
            <a href="#" class="scroll-down" address="true"></a>
        </div>
    </div>
    <div id="publications" class="row pt-5 mt-5 px-1" >
        @foreach ($publications as $publication)
        
            <div class="col-md-4 col-lg-3 col-sm-6 col-12 p-3 pt-5">
                <div class="publication-card">
                    <div class="img-container">
                        <img class="card-img-top" src="/storage/{{ $publication->cover_image }}" alt="Card image cap">          
                    </div>
                    <div class="card-details">
                        <h4 class="card-title">{{ $publication->title }}</h4>
                        <div class="p-container">
                            <p class="card-text text-break text-wrap">{{ $publication->description }}</p>
                        </div>
                        <div class="d-flex justify-content-center pt-2">
                            <a href="/publication/{{ $publication->id }}" class="btn btn-outline-secondary rounded-pill">Read now</a>
                        </div>
                    </div>
                </div>
                <div class="d-flex align-items-top pt-2">
                    <a class="text-secondary" href="/wall/{{ $publication->user->id }}"><img class="rounded-circle img-thumbnail mr-2" src="{{ $publication->user->wall->display_picture() }}" style=" width:50px; height:50px;" alt="DP"></a>
                    <div>
                        <a href="/publication/{{ $publication->id }}" class="text-decoration-none"><h6 class="card-title orange mb-0"><strong>{{ $publication->title }}</strong></h6></a>
                        
                        <a class="text-secondary" href="/wall/{{ $publication->user->id }}"><h6>{{ $publication->user->name }}</h6></a>
                        <ul class="d-flex list-unstyled">
                            <li class="pr-1"><i class="fa fa-eye"></i></li>12
                            <li class="pl-3 pr-1"><i class="far fa-comment"></i></li>0
                            <li class="pl-3 pr-1"><i class="far fa-bookmark"></i></li>{{ $publication->chapters->count() }}
                            
                        </ul>
                    </div>
                </div>
            </div>
      
        @endforeach
        {{-- {{ $publications->links() }} --}}
    </div>
</div>
</div>
@endsection
