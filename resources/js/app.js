/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('follow-button', require('./components/FollowButton.vue').default);
Vue.component('search-bar', require('./components/SearchBar.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});



$(function () {
    $('.scroll-down').click(function () {
        $('html, body').animate({ scrollTop: $('#publications').offset().top }, 'slow');
        return false;
    });
});



const animationEnter = (container) => {
    return gsap.from(container, {
        duration: 1,
        y: 50,
        autoAlpha: 0,
        stagger: 0.1,
        ease: 'power1.in'
    });
}

const animationLeave = (container) => {
    return gsap.to(container, {
        duration: 1,
        y: 50,
        autoAlpha: 0,
        stagger: 0.1,
        ease: 'power1.in'
    });
}

barba.init({
    transitions: [{
        // once({ next }) {
        //     animationEnter(next.container)
        // },
        leave({ current }) {
            animationLeave(current.container)
        },
        enter({ next }) {
            animationEnter(next.container)
        },
    }]

})

let pageNumber = document.getElementsByClassName('swiper-slide');
let pages = [];

for (let count = 1; count <= pageNumber.length; count++) {

    pages.push(count);

};

const btnRight = document.getElementById('slideRight');
const btnLeft = document.getElementById('slideLeft');

if (btnRight) {

    btnRight.onclick = function () {
        document.getElementById('pagination-container').scrollLeft += 300;
    };
}
if (btnLeft) {

    btnLeft.onclick = function () {
        document.getElementById('pagination-container').scrollLeft -= 300;
    };
}

const swiper = new Swiper('.swiper-container', {
    // Optional parameters


    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
        renderBullet: function (index, className) {
            return '<div class="mx-1 my-2 ' + className + '">Page&nbsp' + (pages[index]) + '</div>';
        },
    },

    // Navigation arrows
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },

    // And if we need scrollbar
    scrollbar: {
        el: '.swiper-scrollbar',
    },
    effect: 'slide',
    slideActiveClass: 'swiper-slide-active',
    rotate: 20,
    stretch: 0,
    depth: 200,
    modifier: 1,
    slideShadows: true,
    keyboard: {
        enabled: true,
        onlyInViewport: false,
    },


});


let turnCaretOne = document.querySelector('.turn-caret-one');

if (turnCaretOne) {

    turnCaretOne.addEventListener('click', function () {
        const icon = this.querySelector('.caret-one');

        if (icon.classList.contains('fa-caret-down')) {
            icon.classList.remove('fa-caret-down');
            icon.classList.add('fa-caret-up');
        } else {
            icon.classList.remove('fa-caret-up');
            icon.classList.add('fa-caret-down');
        }
    });
}
const swiperHome = new Swiper('.swiper-container', {
    // loop: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    slidesPerView: 3,
    spaceBetween: 20,


});


// let pathArray = window.location.pathname.split('/');
// let secondLevelLocation = pathArray[1];

let home = document.querySelector('.home');
let discover = document.querySelector('.discover');
let about = document.querySelector('.about');
let contact = document.querySelector('.contact');



var oldURL = "";
var currentURL = window.location.href;
function checkURLchange(currentURL) {
    if (currentURL != oldURL) {
        // console.log(currentURL);
        if (currentURL === "http://localhost:3000/contact") {
            if (!contact.classList.contains('active-nav')) {
                contact.classList.add('active-nav');
                about.classList.remove('active-nav');
                home.classList.remove('active-nav');
                discover.classList.remove('active-nav');
            }
        } else if (currentURL === "http://localhost:3000/about") {
            if (!about.classList.contains('active-nav')) {
                about.classList.add('active-nav');
                contact.classList.remove('active-nav');
                home.classList.remove('active-nav');
                discover.classList.remove('active-nav');
            }
        } else if (currentURL === "http://localhost:3000/") {
            if (!discover.classList.contains('active-nav')) {
                discover.classList.add('active-nav');
                contact.classList.remove('active-nav');
                home.classList.remove('active-nav');
                about.classList.remove('active-nav');
            }
        } else if (currentURL === "http://localhost:3000/home") {
            if (!home.classList.contains('active-nav')) {
                home.classList.add('active-nav');
                contact.classList.remove('active-nav');
                about.classList.remove('active-nav');
                discover.classList.remove('active-nav');
            }
        } else {
            home.classList.remove('active-nav');
            contact.classList.remove('active-nav');
            about.classList.remove('active-nav');
            discover.classList.remove('active-nav');
        }
        oldURL = currentURL;
    }

    oldURL = window.location.href;
    setTimeout(function () {
        checkURLchange(window.location.href);
    }, 1000);
}

checkURLchange();


const chooseFile = document.getElementById("choose-file-cover-image");
const imgPreview = document.getElementById("img-preview");
const chooseFileEdit = document.getElementById("choose-file-edit");
const imgPreviewEdit = document.getElementById("img-preview-edit");
const chooseFileWallEdit = document.getElementById("choose-file-display-picture");
const imgPreviewWallEdit = document.getElementById("img-preview-display-picture");


if (chooseFile) {
    chooseFile.addEventListener("change", function () {
        getImgData(chooseFile, imgPreview);
    });
}
if (chooseFileEdit) {
    chooseFileEdit.addEventListener("change", function () {
        getImgData(chooseFileEdit, imgPreviewEdit);
    });
}
if (chooseFileWallEdit) {
    chooseFileWallEdit.addEventListener("change", function () {
        getImgDataWall(chooseFileWallEdit, imgPreviewWallEdit);
    });
}

function getImgData(c, i) {
    const files = c.files[0];
    if (files) {
        const fileReader = new FileReader();
        fileReader.readAsDataURL(files);
        fileReader.addEventListener("load", function () {
            i.style.display = "block";
            i.innerHTML = '<img class="w-100" src="' + this.result + '" alt="img" />';
        });
    }
}
function getImgDataWall(c, i) {
    const files = c.files[0];
    if (files) {
        const fileReader = new FileReader();
        fileReader.readAsDataURL(files);
        fileReader.addEventListener("load", function () {
            i.style.display = "block";
            i.innerHTML = ' <div class="dp-container d-flex justify-content-center align-items-center rounded-circle" style="width:200px; height:200px; overflow:hidden;"><img class="w-100" src="' + this.result + '" alt="display picture" /></div>';

        });
    }
}

$('.search-button').click(function () {
    $(this).parent().toggleClass('open');
});


