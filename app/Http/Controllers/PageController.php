<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Page;
use \App\Models\Publication;
use \App\Models\Chapter;

class PageController extends Controller
{
    public function edit(Publication $publication, Chapter $chapter, Page $page){
        return view('publications.chapters.pages.edit', compact('publication', 'chapter', 'page'));
    }

    public function update(Request $request, $publication, $chapter, $page){
        // dd(auth()->user()->publications()->find($publication)->chapters()->find($chapter)->pages->find($page));
        if ($_FILES['file']['name']) {
            $request->validate([
                'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
              ]);
            if (!$_FILES['file']['error']) {
                $name = md5(rand(100, 200));
                $ext = explode('.', $_FILES['file']['name']);
                $filename = $name . '.' . $ext[1];
                $destination = public_path() . '/images/' . $filename;
                $location = $_FILES["file"]["tmp_name"];
                move_uploaded_file($location, $destination);
                echo '/images/' . $filename;
            } else {
                echo 'Ooops!  Your upload triggered the following error:  '.$_FILES['file']['error'];
            }
          }
          $pages = auth()->user()->publications()->find($publication)->chapters()->find($chapter)->pages->find($page)->update([
            'page_path' => '/images/' . $filename,
            
        ]);
    }

    public function destroy(Publication $publication, Chapter $chapter, $page){
        
        Page::destroy($page);

        return redirect()->route('chapter.edit', ['publication' => $publication, 'chapter' => $chapter->id]);
    }
}
