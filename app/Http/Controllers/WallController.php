<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Intervention\Image\Facades\Image;
use App\Models\Publication;


class WallController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    // The User inside the parentheses refers to "\App\Models\User", the App\Models was removed because it is being imported above.
    public function index(User $user)
    {   
        $follows = (auth()->user()) ? auth()->user()->following->contains($user->id) : false;

        $publicationCount =  Cache::remember('count.publications'.$user->id, now()->addSeconds(1), function () use ($user) {
            return $user->publications->count();
        });

        $followingCount =  Cache::remember('count.following'.$user->id, now()->addSeconds(1), function () use ($user) {
            return $followingCount = $user->following->count();
        });

        $followersCount =  Cache::remember(''.$user->id, now()->addSeconds(1), function () use ($user) {
            return $followersCount = $user->wall->followers->count();
        });

        

        
        return view('walls/index', compact('user','follows','publicationCount','followingCount','followersCount'));
    }



    public function edit(User $user){
        $this->authorize('update', $user->wall);
        // In this case the compact methode and an array do the exact same thing.
        return view('walls.edit', compact('user'));

    }
    public function update(User $user){

        $this->authorize('update', $user->wall);
        $data = request()->validate([
            'wall_title' => 'required',
            'bio' => 'required',
            'display_picture' => 'image|mimes:jpg,png,jpeg,gif,svg|max:5000',
            'manga' => 'boolean',
            'comics' => 'boolean',
            'belgian_comics' => 'boolean',
            'comedy' => 'boolean',
            'action' => 'boolean',
            'adventure' => 'boolean',
            'gore' => 'boolean',
            'fantasy' => 'boolean',
            'sci_fi' => 'boolean',
            'sport' => 'boolean',
        ]);

        if(request('display_picture')){
            $imagePath = request('display_picture')->store('walls','public');
            // this is to cut images before saving them
            $image = Image::make(public_path("storage/{$imagePath}"))->fit(500,500);
            $image->save();
            $imageArray = ['display_picture' =>  $imagePath ];
        }
   
        auth()->user()->wall->update(array_merge(
            $data,
            $imageArray ?? [],
        ));
        
        return redirect("/wall/{$user->id}");
    }
}
