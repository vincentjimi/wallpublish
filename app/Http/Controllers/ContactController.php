<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ContactMail;
use Mail;

class ContactController extends Controller
{
    public function contact(){
        return view('contact');
    }

    public function sendEmail(Request $request){
        
        $data = request()->validate([
            // for fields that don't require any validation, put empty quotes after the =>
            'name' => 'required',
            'email' => 'required|email',
            'subject' => 'required',
            'message' => 'required',
        ]);
        $details = [
            'name' => $data['name'],
            'email' => $data['email'],
            'subject' => $data['subject'],
            'message' => $data['message']
        ];

        Mail::to('info@wallpublish.com')->send(new ContactMail($details));
        return back()->with('message_sent', 'Your message has been sent successfully ! Thank you for reaching out to us, we will get back to you as soon as possible.');
    }
}
