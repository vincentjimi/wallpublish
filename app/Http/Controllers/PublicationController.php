<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use \App\Models\Publication;

class PublicationController extends Controller
{
    public function __construct(){

        $this->middleware('auth', ['only' => ['create']]);
    }

    public function search(){

        return Publication::all();
    }
    

    public function create(){
        

        return view('publications/create');
    }
    
    

    public function store(){
        $data = request()->validate([
            // for fields that don't require any validation, put empty quotes after the =>
            'title' => 'required',
            'description' => 'required|min:10|max:500',
            'cover_image' => ['required','image'],
            'manga' => 'boolean',
            'comics' => 'boolean',
            'belgian_comics' => 'boolean',
            'comedy' => 'boolean',
            'action' => 'boolean',
            'adventure' => 'boolean',
            'gore' => 'boolean',
            'fantasy' => 'boolean',
            'sci_fi' => 'boolean',
            'sport' => 'boolean',
        ]);

        // dd();
        $imagePath = request('cover_image')->store('uploads','public');
        // this is to cut images before saving them
        $image = Image::make(public_path("storage/{$imagePath}"))->fit(600,900);
        $image->save();

        if(request('manga')){
            $checkboxArray_manga = [
                'manga' => $data['manga'],
            ];
        }
        if(request('comics')){
            $checkboxArray_comics = [
                'comics' => $data['comics'],
            ];
        }
        if(request('belgian_comics')){
            $checkboxArray_belgian_comics = [
                'belgian_comics' => $data['belgian_comics'],
            ];
        }
        if(request('comedy')){
            $checkboxArray_comedy = [
                'comedy' => $data['comedy'],
            ];
        }
        if(request('action')){
            $checkboxArray_action = [
                'action' => $data['action'],
            ];
        }
        if(request('adventure')){
            $checkboxArray_adventure = [
                'adventure' => $data['adventure'],
            ];
        }
        if(request('gore')){
            $checkboxArray_gore = [
                'gore' => $data['gore'],
            ];
        }
        if(request('fantasy')){$checkboxArray_fantasy = [
            'fantasy' => $data['fantasy'],
        ];}
        if(request('sci_fi')){$checkboxArray_sci_fi = [
            'sci_fi' => $data['sci_fi'],
        ];}
        if(request('sport')){ $checkboxArray_sport = [
            'sport' => $data['sport'],
        ];}
        
        $imageArray_cover_image = [
            'cover_image' => $imagePath
        ];
   
        $fullData = array_merge(
            $data,
            $checkboxArray_manga ?? [],
            $checkboxArray_comics ?? [],
            $checkboxArray_belgian_comics ?? [],
            $checkboxArray_comedy ?? [],
            $checkboxArray_action ?? [],
            $checkboxArray_adventure ?? [],
            $checkboxArray_gore ?? [],
            $checkboxArray_fantasy ?? [],
            $checkboxArray_sci_fi ?? [],
            $checkboxArray_sport ?? [],
            $imageArray_cover_image ?? []
        );

        $newPublication = auth()->user()->publications()->create($fullData);
        // return redirect('/wall/'.auth()->user()->id);
        return redirect()->route('publication.show', $newPublication);
     }
     

     public function show(Publication $publication){

        Publication::increment('views');
        return view('publications/show', compact('publication'));
     }


     public function edit(Publication $publication){
        
        $this->authorize('update', $publication);
        return view('publications/edit', compact('publication'));
     }

   
     public function update(Publication $publication){

        $this->authorize('update', $publication);
        
        $data = request()->validate([
            // for fields that don't require any validation, put empty quotes after the =>
            'title' => 'required',
            'description' => 'required|min:10|max:500',
            'cover_image' => 'image',
            ]);
            
            
            if(request('cover_image')){
            $imagePath = request('cover_image')->store('uploads','public');
            // this is to cut images before saving them
            $image = Image::make(public_path("storage/{$imagePath}"))->fit(600,900);
            $image->save();
            $imageArray = ['cover_image' =>  $imagePath ];
            
        }
        
        auth()->user()->publications->find($publication)->update(array_merge(
            $data,
            $imageArray ?? [],
        ));
        
        return redirect("/publication/{$publication->id}");
    }
    public function destroy(Publication $publication){
        $this->authorize('delete', $publication);
        
        Publication::destroy($publication->id);
        
        $user = auth()->user();
        
        return redirect()->route('wall.show', $user);
    }

    
}
