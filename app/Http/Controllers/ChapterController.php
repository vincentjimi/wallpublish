<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Chapter;
use \App\Models\Publication;
use Illuminate\Http\UploadedFile;


class ChapterController extends Controller
{
    public function __construct(){

        // $this->middleware('auth');
    }
    
       
       
    public function store($publication){
        
        // dd($publication);
        $data = request()->validate([
            'chapter_name' => 'required|min:2|max:26',
            // 'summary' => '',
        ]);

        $newChapter = auth()->user()->publications()->find($publication)->chapters()->create([
            'chapter_name' => $data['chapter_name'],
            // 'summary' => $data['summary'],
        ]);
        
        // return redirect('/wall/'.auth()->user()->id);
        // dd($publication,$newChapter->id);
        return redirect()->route('chapter.show', ['publication' => $publication, 'chapter' => $newChapter->id]);
        
     }

     public function show(Publication $publication, Chapter $chapter){
        // dd($publication, $chapter);
        
        return view('\publications\chapters\show', compact('publication','chapter'));
     }

     public function upload(Request $request, $publication, $chapter)
    {   
        if ($_FILES['file']['name']) {
            $request->validate([
                'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
              ]);
            if (!$_FILES['file']['error']) {
                $name = md5(rand(100, 200));
                $ext = explode('.', $_FILES['file']['name']);
                $filename = $name . '.' . $ext[1];
                $destination = public_path() . '/images/' . $filename;
                $location = $_FILES["file"]["tmp_name"];
                move_uploaded_file($location, $destination);
                echo '/images/' . $filename;
            } else {
                echo 'Ooops!  Your upload triggered the following error:  '.$_FILES['file']['error'];
            }
          }
          
          $pages = auth()->user()->publications()->find($publication)->chapters()->find($chapter)->pages()->create([
            'page_path' => '/images/' . $filename,
            
        ]);

    }
    

    public function edit(Publication $publication, Chapter $chapter){
        // dd(\App\Models\Chapter::find($chapter)->publication->user_id);
        // $this->authorize('update', \App\Models\Chapter::find($chapter)->publication->user_id);
        return view('publications/chapters/edit', compact('publication', 'chapter'));
     }

   
     public function update(Publication $publication, Chapter $chapter){

        // $this->authorize('update', $chapter);
        
        $data = request()->validate([
            'chapter_name' => 'required',
        ]);


        auth()->user()->publications->find($publication)->chapters->find($chapter)->update($data);
  
        return redirect("/publication/{$publication->id}");
    }
    public function destroy(Publication $publication, Chapter $chapter){
        
        Chapter::destroy($chapter->id);

        return redirect()->route('publication.show', $publication);
    }

}
