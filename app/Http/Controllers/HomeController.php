<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Publication;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $users = auth()->user()->following()->pluck('walls.user_id');
        $publications = Publication::whereIn('user_id', $users)->with('user')->latest()->paginate(30);



    // ------------- Selecting bolean value from user wall to know user's interests---------------------------

        $var_manga = auth()->user()->wall->manga;
        $var_comics = auth()->user()->wall->comics;
        $var_belgian_comics = auth()->user()->wall->belgian_comics;
        $var_comedy = auth()->user()->wall->comedy;
        $var_action = auth()->user()->wall->action;
        $var_adventure = auth()->user()->wall->adventure;
        $var_gore = auth()->user()->wall->gore;
        $var_fantasy = auth()->user()->wall->fantasy;
        $var_sci_fi = auth()->user()->wall->sci_fi;
        $var_sport = auth()->user()->wall->sport;
    
    // ------------- Check if user is interested -and search for publication on that  condition--------------------

        if($var_manga){
            $interested_manga = Publication::all()->where('manga', $var_manga);

        }
        if($var_comics){
            $interested_comics = Publication::all()->where('comics', $var_comics);

        }
        if($var_belgian_comics){
            $interested_belgian_comics = Publication::all()->where('belgian_comics', $var_belgian_comics);

        }
        if($var_comedy){
            $interested_comedy = Publication::all()->where('comedy', $var_comedy);

        }
        if($var_action){
            $interested_action = Publication::all()->where('action', $var_action);

        }
        if($var_adventure){
            $interested_adventure = Publication::all()->where('adventure', $var_adventure);
        }
        if($var_gore){
            $interested_gore = Publication::all()->where('gore', $var_gore);

        }
        if($var_fantasy){
            $interested_fantasy = Publication::all()->where('fantasy', $var_fantasy);

        }
        if($var_sci_fi){
            $interested_sci_fi = Publication::all()->where('sci_fi', $var_sci_fi);

        }
        if($var_sport){
            $interested_sport = Publication::all()->where('sport', $var_sport);

        }
// -------------------------------------Make unions-------------------------------------------------------

        if(isset($interested_manga)){
            $v1 = $interested_manga;
        }
        if(isset($interested_comics)){
            if(isset($v1)){
                $v2 = $v1->union($interested_comics);
            }else{
                $v2 = $interested_comics;
            }
        }
        if(isset($interested_belgian_comics)){
            if(isset($v2)){
                $v3 = $v2->union($interested_belgian_comics);
            }elseif(isset($v1)){
                $v3 = $v1->union($interested_belgian_comics);
            }else{
                $v3 = $interested_belgian_comics;
            }
        }
        if(isset($interested_comedy)){
            if(isset($v3)){
                $v4 = $v3->union($interested_comedy);
            }else if(isset($v2)){
                $v4 = $v2->union($interested_comedy);
            }else if(isset($v1)){
                $v4 = $v1->union($interested_comedy);
            }else{
                $v4 = $interested_comedy;
            }
        }
        if(isset($interested_action)){
            if(isset($v4)){
                $v5 = $v4->union($interested_action);
            }else if(isset($v3)){
                $v5 = $v3->union($interested_action);
            }else if(isset($v2)){
                $v5 = $v2->union($interested_action);
            }else if(isset($v1)){
                $v5 = $v1->union($interested_action);
            }else{
                $v5 = $interested_action;
            }
        }
        if(isset($interested_adventure)){
            if(isset($v5)){
                $v6 = $v5->union($interested_adventure);
            }else if(isset($v4)){
                $v6 = $v4->union($interested_adventure);
            }else if(isset($v3)){
                $v6 = $v3->union($interested_adventure);
            }else if(isset($v2)){
                $v6 = $v2->union($interested_adventure);
            }else if(isset($v1)){
                $v6 = $v1->union($interested_adventure);
            }else{
                $v6 = $interested_adventure;
            }
        }
        if(isset($interested_gore)){
            if(isset($v6)){
                $v7 = $v6->union($interested_gore);
            }else if(isset($v5)){
                $v7 = $v5->union($interested_gore);
            }else if(isset($v4)){
                $v7 = $v4->union($interested_gore);
            }else if(isset($v3)){
                $v7 = $v3->union($interested_gore);
            }else if(isset($v2)){
                $v7 = $v2->union($interested_gore);
            }else if(isset($v1)){
                $v7 = $v1->union($interested_gore);
            }else{
                $v7 = $interested_gore;
            }
        }
        if(isset($interested_fantasy)){
            if(isset($v7)){
                $v8 = $v7->union($interested_fantasy);
            }else if(isset($v6)){
                $v8 = $v6->union($interested_fantasy);
            }else if(isset($v5)){
                $v8 = $v5->union($interested_fantasy);
            }else if(isset($v4)){
                $v8 = $v4->union($interested_fantasy);
            }else if(isset($v3)){
                $v8 = $v3->union($interested_fantasy);
            }else if(isset($v2)){
                $v8 = $v2->union($interested_fantasy);
            }else if(isset($v1)){
                $v8 = $v1->union($interested_fantasy);
            }else{
                $v8 = $interested_fantasy;
            }

        }
        if(isset($interested_sci_fi)){
            if(isset($v8)){
                $v9 = $v8->union($interested_sci_fi);
            }else if(isset($v7)){
                $v9 = $v7->union($interested_sci_fi);
            }else if(isset($v6)){
                $v9 = $v6->union($interested_sci_fi);
            }else if(isset($v5)){
                $v9 = $v5->union($interested_sci_fi);
            }else if(isset($v4)){
                $v9 = $v4->union($interested_sci_fi);
            }else if(isset($v3)){
                $v9 = $v3->union($interested_sci_fi);
            }else if(isset($v2)){
                $v9 = $v2->union($interested_sci_fi);
            }else if(isset($v1)){
                $v9 = $v1->union($interested_sci_fi);
            }else{
                $v9 = $interested_sci_fi;
            }
        }

        if(isset($interested_sport)){
            if(isset($v9)){
                $v10 = $v9->union($interested_sport);
            }else if(isset($v8)){
                $v10 = $v8->union($interested_sport);
            }else if(isset($v7)){
                $v10 = $v7->union($interested_sport);
            }else if(isset($v6)){
                $v10 = $v6->union($interested_sport);
            }else if(isset($v5)){
                $v10 = $v5->union($interested_sport);
            }else if(isset($v4)){
                $v10 = $v4->union($interested_sport);
            }else if(isset($v3)){
                $v10 = $v3->union($interested_sport);
            }else if(isset($v2)){
                $v10 = $v2->union($interested_sport);
            }else if(isset($v1)){
                $v10 = $v1->union($interested_sport);
            }else{
                $v10 = $interested_sport;
            }

        }

        if(isset($v10)){$interested = $v10;
        }else if(isset($v9)){$interested = $v9;
        }else if(isset($v8)){$interested = $v8;
        }else if(isset($v7)){$interested = $v7;
        }else if(isset($v6)){$interested = $v6;
        }else if(isset($v5)){$interested = $v5;
        }else if(isset($v4)){$interested = $v4;
        }else if(isset($v3)){$interested = $v3;
        }else if(isset($v2)){$interested = $v2;
        }else if(isset($v1)){$interested = $v1;
        }else{$interested = collect([]); ;}

        return view('home', compact('publications', 'interested'));
    }
}
