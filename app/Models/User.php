<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewUserWelcomeMail;
use Laravelista\Comments\Commenter;

class User extends Authenticatable
{
    use HasFactory, Notifiable, Commenter;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected static function boot(){

        parent::boot();
        static::created(
            function($user){
                $user->wall()->create([
                    'wall_title' => $user->name."'s Wall",
                ]);
                Mail::to($user->email)->send(new NewUserWelcomeMail());
            }

        );
    }
    public function following(){
        return $this->belongsToMany(Wall::class);
    }

    public function publications(){
        return $this->hasMany(Publication::class)->orderBy('created_at', 'DESC');
    }

    public function wall(){
        return $this->hasOne(Wall::class);
    }
}
