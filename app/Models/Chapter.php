<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravelista\Comments\Commentable;

class Chapter extends Model
{
    use HasFactory, Commentable;
    protected $guarded = [];
    
    public function pages(){
        return $this->hasMany(Page::class);
    }
    public function publication(){
        return $this->belongsTo(Publication::class);
    }
}
