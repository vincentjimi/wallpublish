<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wall extends Model
{
    use HasFactory;
    
    protected $guarded = [];

    public function display_picture(){

        $default_image_path = ($this->display_picture) ? $this->display_picture : "walls/newUser.svg";
        return '/storage/' . $default_image_path;
    }

    public function followers(){
        return $this->belongsToMany(User::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
