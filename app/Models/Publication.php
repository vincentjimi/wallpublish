<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravelista\Comments\Commentable;

class Publication extends Model
{
    use HasFactory, Commentable;

    protected $guarded = [];
    

    public function chapters(){
        return $this->hasMany(Chapter::class);
    }

    public function user(){
        return $this->belongsTO(User::class);
    }

}
