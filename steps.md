## 1 This is to creat a new project
## laravel new <project name>

## 2 This is to serve your page in the browser
# php artisan serve

## 3 This is to create user accounts
# composer require laravel/ui --dev
# php artisan ui vue --auth
# php artisan migrate

## 4 This is to install npm package manager
# npm install

## 5 To use the packages
### Might need to put the project on the desktop for it to work
# npm run dev
## For automatic compiling ex. scss to css
# npm run watch
## 6 For auto reload add this to the webpack.mix.js file
### mix.browserSync('127.0.0.1:8000');  